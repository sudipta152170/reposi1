#include <stdio.h>
#include <stdlib.h>

int main()
{
    int a,b,sum1,sum2,sum3;
    float sum4;
    printf("Inter 1st & 2nd value");
    scanf("%d %d", &a, &b);
    sum1=a+b;
    sum2=a-b;
    sum3=a*b;
    sum4=a/(float)b;
    printf("Sum = %d , Difference = %d , Multiplication = %d , Division = %.2f", sum1,sum2,sum3,sum4);
    return 0;
}

